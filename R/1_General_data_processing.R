#' Plot numbers of identified proteins
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names. Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @return Returns a ggplot object, which summarizes the different counts and can be viewed using print().
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #plot numbers of identified proteins in replicates
#' gg = plot_count(sampleTable = dat_FC_replicates)
#' print(gg)
#'
#' ##plot numbers of identified proteins in processed data (e.g. average FCs)
#' #dat_average_FCs = dat_calculated_LFQ[, grepl(colnames(dat_calculated_LFQ), pattern = "_FC$")]
#' #gg = plot_count(sampleTable = dat_average_FCs)
#' #print(gg)
#'
#' setwd(savewd)
#' @export
plot_count <- function(sampleTable){

  tryCatch({

  dat_to_plot = sampleTable
  dat_count = apply(dat_to_plot, 2, function(x){
    sum(!is.na(x))
  })

  dat_to_plot_long = as.data.frame(dat_count)
  dat_to_plot_long$Sample = rownames(dat_to_plot_long)

  max_count = plyr::round_any(max(dat_to_plot_long$dat_count), 500, f = ceiling)

  dat_to_plot_long$Class = gsub("(.*)_.*", "\\1", row.names(dat_to_plot_long))
  dat_to_plot_long$Count = dat_count

  k = length(unique(dat_to_plot_long$Class))
  npg_colors = ggpubr::get_palette(palette="npg", k=k)

  gg = ggplot2::ggplot(data=dat_to_plot_long, ggplot2::aes(x=dat_to_plot_long$Sample,
                                                           y = dat_to_plot_long$Count,
                                                           fill = dat_to_plot_long$Class)) +
    ggplot2::geom_bar(stat = "identity", show.legend = FALSE) +
    ggplot2::scale_fill_manual(values = npg_colors) +
    ggplot2::scale_y_continuous(limits = c(0, max_count), expand = c(0, 0),
                                breaks = c(0, max_count/2, max_count)) +
    ggplot2::theme_classic() +
    ggplot2::ylab("#") +
    ggplot2::theme(axis.title.y = ggplot2::element_text(size = 8, color = "black", face = "plain"),
                   axis.title.x = ggplot2::element_blank(),
                   axis.text.x = ggplot2::element_text(size = 8, color = "black", face = "plain",
                                                       angle = 45, vjust = 1, hjust = 1),
                   axis.text.y = ggplot2::element_text(size = 6, color = "black", hjust = 1, face = "plain"),
                   axis.line = ggplot2::element_blank(),
                   axis.ticks = ggplot2::element_line(colour = "black", linewidth = 0.5),
                   legend.title = ggplot2::element_blank(),
                   plot.margin = ggplot2::margin(t = 0.2, r = 0.2, b = 0.2, l = 0.5, "cm"),
                   panel.border = ggplot2::element_rect(fill = NA, colour = "black", linewidth = 0.5)
    )

  return(gg)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })
}

#' Sample-2-Sample distance plot
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param color A vector of two colors, which should be used for the color gradient of the resulting heatmap. Default: c("#3C5488FF", "white")
#' @param show_dends TRUE of FALSE. Decide whether the row dendrogram and the column dendrogram should be shown.
#' @returns A clustered heatmap illustrating the results, which can be viewed using print(). The function uses dist() to evaluate differences between samples.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #plot sample-2-sample distances for replicate data using the default color code
#' ht = S2S_plot(sampleTable = dat_FC_replicates)
#' print(ht)
#'
#' ##plot sample-2-sample distances for replicate data specifying colors
#' #ht = S2S_plot(sampleTable = dat_FC_replicates, color = c("red", "yellow"))
#' #print(ht)
#'
#' setwd(savewd)
#' @export
S2S_plot <- function(sampleTable, color = c("#3C5488FF", "white"),
                     show_dends = TRUE) {

  tryCatch({

  my_palette = grDevices::colorRampPalette(color)(255)

  #transpose data for sample2sample distance plotting
  x = t(sampleTable)

  m = as.matrix(stats::dist(x))

  ht = ComplexHeatmap::Heatmap(m,
                               col = my_palette,
                               color_space = "sRGB",
                               #na_col="grey",
                               cluster_rows = TRUE,
                               cluster_columns= TRUE,
                               column_dend_side="top",
                               column_dend_height= grid::unit(3,"cm"),
                               column_names_side="top",
                               show_row_names = TRUE,
                               row_names_side = "left",
                               # row_names_max_width = grid::unit(10, "cm"),
                               # column_names_max_height = grid::unit(10, "cm"),
                               show_row_dend = show_dends,
                               show_column_dend = show_dends,
                               row_dend_width = grid::unit(3,"cm"),
                               row_names_gp = grid::gpar(fontsize = 8, fontface = "plain"),
                               column_names_gp = grid::gpar(fontsize = 8, fontface = "plain"),
                               #column_dend_reorder=c(1,3,4,5,2),
                               #row_dend_reorder=F,
                               #split = find_rows$k,
                               gap = grid::unit(2, "mm"),
                               # heatmap_legend_param = list(title = "-Log10(FC)",
                               #                             title_gp = gpar(cex = 1, fontface = "bold"),
                               #                             color_bar = c("discrete"),
                               #                             at = c(0, 0.1, 1.3),
                               #                             labels = c("NA", "n.s.", "*")),
                               show_heatmap_legend=FALSE)

  return(ht)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Evaluate reproducibility of replicates
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix, since this is how the replicate data for the different conditions are selected). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param suffix Define a suffix for the filenames of the output plots. This is helpful if this function is applied several times within one script/folder. Default: NA (no suffix is added)
#' @returns This function will produce some output, which will be saved to a folder called "Plots" within your working directory. Exports plots of PCAs and correlation bubble plots to evaluate the reproducibility of the replicates.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #perform reproducibility check for replicate data before any processing
#' #The output will be stored to a subfolder named "Plots" without any filename suffix
#' QC_check(dat_FC_replicates)
#'
#' ##perform reproducibility check for replicate data after processing
#' ##(e.g. after log2 transformation, variance stabilization)
#' ##The output will be stored to a subfolder named "Plots"
#' ##the suffix "processed" is added to the filenames
#' ##QC_check(dat_replicates_processed, suffix = "processed")
#'
#' setwd(savewd)
#' @export
QC_check <- function(sampleTable, suffix = NA) {

  tryCatch({

  samples = unique(gsub("(.*)_.*","\\1", colnames(sampleTable)))

  for ( i in samples){

    tryCatch({

    dat_subset = subset(sampleTable, select = grepl(pattern = i,
                                                    colnames(sampleTable)))

    colnames(dat_subset) = sub(".*\\_", "", colnames(dat_subset))

    dat_subset[is.na(dat_subset)] = 0

    PCA.proteins = as.matrix(dat_subset)
    PCA.proteins = t(PCA.proteins)

    PCA.proteins = PCA.proteins[ , colSums(is.na(PCA.proteins)) == 0]

    ncomp = ifelse(floor(ncol(dat_subset)/4) >=2, floor(ncol(dat_subset)/4), 2)
    PCA.PCA = mixOmics::pca(PCA.proteins, ncomp=ncomp)

    #Plots
    create_folder = paste(getwd(), "Plots", sep = "/")
    if (!dir.exists(create_folder)){
      dir.create(create_folder, showWarnings = FALSE)
    }

    plots_dir = create_folder

    if(!is.na(suffix)){
      plotname = paste("PCA_replicates_", suffix, "_", i, sep = "")
    }

    if(is.na(suffix)){
      plotname = paste("PCA_replicates_", i, sep = "")
    }

    filename = paste(plots_dir, plotname, sep = "/")
    plotfile = paste(filename, ".png", sep = "")

    grDevices::png(file = plotfile,
                   units = "in",
                   res = 800,
                   width = 5,
                   height = 5)

    #plot
    mixOmics::plotIndiv(PCA.PCA,
                        comp= c(1,2),
                        ind.names = TRUE,
                        legend = FALSE,
                        col = "black",
                        ellipse = FALSE,
                        title = "",
                        star=FALSE,
                        style="graphics")

    grDevices::dev.off()

    plotfile = paste(filename, ".pdf", sep = "")

    grDevices::pdf(file = plotfile,
                   width = 5,
                   height = 5)

    #plot
    mixOmics::plotIndiv(PCA.PCA,
                        comp= c(1,2),
                        ind.names = TRUE,
                        legend = FALSE,
                        col = "black",
                        ellipse = FALSE,
                        title = "",
                        star=FALSE,
                        style="graphics")

    grDevices::dev.off()

    remove(plotname)
    remove(plotfile)

    res = stats::cor(dat_subset, use = "complete")

    if(!is.na(suffix)){
      plotname = paste("CorrelationBubbles_replicates_", suffix, "_", i, sep = "")
    }

    if(is.na(suffix)){
      plotname = paste("CorrelationBubbles_replicates_", i, sep = "")
    }

    filename = paste(plots_dir, plotname, sep = "/")
    plotfile = paste(filename, ".png", sep = "")

    grDevices::png(file = plotfile,
                   units = "in",
                   res = 800,
                   width = 5,
                   height = 5)

    corrplot::corrplot(res,
                       type = "upper",
                       tl.col = "black",
                       sig.level = 0.05,
                       tl.srt = 45)

    grDevices::dev.off()

    plotfile = paste(filename, ".pdf", sep = "")

    grDevices::pdf(file = plotfile,
                   width = 5,
                   height = 5)

    corrplot::corrplot(res,
                       type = "upper",
                       tl.col = "black",
                       sig.level = 0.05,
                       tl.srt = 45)

    grDevices::dev.off()

    remove(plotfile)
    remove(plotname)

    }, error = function(e) {
      cat("ERROR:", conditionMessage(e), i, "\n")
    })

  }

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Outlier identification
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param pvalue_cutoff Define the p-value, which should be used to consider samples to be outliers. Default: 0.05
#' @returns Returns a vector of samples, which are outliers based on their Mahalanobis distance and the defined p-value cutoff. PCA is used as basis, and the first two principal components are considered for outlier identification. This function will produce some output, which will be saved to a folder called "Plots" within your working directory.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #identify outliers based on replicate data
#' #use a p-value cutoff of 0.05 to consider the sample to be an outlier
#' get_outliers = identify_outliers(sampleTable = dat_FC_replicates)
#' get_outliers
#'
#' ##identify outliers based on replicate data
#' ##use a p-value cutoff of 0.01 to consider the sample to be an outlier
#' #get_outliers = identify_outliers(sampleTable = dat_FC_replicates, pvalue_cutoff = 0.01)
#' #get_outliers
#'
#' setwd(savewd)
#' @export
identify_outliers <- function(sampleTable, pvalue_cutoff = 0.05) {

  tryCatch({

  dat_subset = sampleTable
  dat_subset[is.na(dat_subset)] = 0

  PCA.proteins = t(as.matrix(dat_subset))
  PCA.proteins = PCA.proteins[ , colSums(is.na(PCA.proteins)) == 0]

  spca = ClassDiscovery::SamplePCA(t(PCA.proteins))

  maha2 = ClassDiscovery::mahalanobisQC(spca, 2)

  get_outliers = (row.names(maha2)[which(maha2$p.value <= pvalue_cutoff)])

  spca = ClassDiscovery::SamplePCA(PCA.proteins)

  PCA_ggplot = as.data.frame(spca@components)
  PCA_ggplot = PCA_ggplot[, 1:2]
  PCA_ggplot$Class = gsub("(.*)_.*", "\\1", row.names(PCA_ggplot))
  PCA_ggplot$Replicate = sub(".*\\_", "", row.names(PCA_ggplot))

  PCA_ggplot$Label = NA
  PCA_ggplot$Label[grepl(rownames(PCA_ggplot), pattern = paste(get_outliers, collapse = "|"))] = PCA_ggplot$Replicate[grepl(rownames(PCA_ggplot), pattern = paste(get_outliers, collapse = "|"))]

  #Plots
  create_folder = paste(getwd(), "Plots", sep = "/")
  if (!dir.exists(create_folder)){
    dir.create(create_folder, showWarnings = FALSE)
  }
  plots_dir = create_folder

  #define color palette
  #create extended color pallette: k colors
  #k equals the number of samples that you have
  k = length(unique(PCA_ggplot$Class))
  npg_colors = ggpubr::get_palette(palette="npg", k=k)

  gg = ggplot2::ggplot(PCA_ggplot, ggplot2::aes(x = PCA_ggplot$PC1,
                                                y = PCA_ggplot$PC2,
                                                color = PCA_ggplot$Class,
                                                label = PCA_ggplot$Label)) +
    ggplot2::geom_point(size = 2) +
    ggplot2::geom_text(ggplot2::aes(label = PCA_ggplot$Label, color = PCA_ggplot$Class), size = 8*5/14,
                       hjust = -0.5, vjust = 0.5) +
    ggplot2::scale_color_manual(values = npg_colors) +
    ggplot2::stat_ellipse(level = 0.8) +
    ggplot2::scale_x_continuous() +
    ggplot2::scale_y_continuous() +
    ggplot2::theme_classic() +
    ggplot2::theme(axis.title.y = ggplot2::element_text(size = 8, color = "black", face = "plain"),
                   axis.title.x = ggplot2::element_text(size = 8, color = "black", face = "plain"),
                   axis.text.x = ggplot2::element_text(size = 6, color = "black", face = "plain"),
                   axis.text.y = ggplot2::element_text(size = 6, color = "black", hjust = 1, face = "plain"),
                   axis.line = ggplot2::element_blank(),
                   axis.ticks = ggplot2::element_line(colour = "black", linewidth = 0.5),
                   legend.title = ggplot2::element_text(size = 8, face = "plain", color = "black"),
                   legend.text = ggplot2::element_text(size = 6, face = "plain", color = "black"),
                   panel.border = ggplot2::element_rect(colour = "black", fill=NA, linewidth=0.5)) +
    ggplot2::ylab("PC2") +
    ggplot2::xlab("PC1") +
    ggplot2::labs(color = "Treatment")

  plotname = "PCA_Mahalanobis_Outliers.pdf"
  filename = paste(plots_dir, plotname, sep = "/")

  grDevices::pdf(
    file = filename,
    width = 5,
    height = 3
  )

  suppressWarnings(print(gg))

  grDevices::dev.off()

  plotname = "PCA_Mahalanobis_Outliers.png"
  filename = paste(plots_dir, plotname, sep = "/")

  grDevices::png(
    file = filename,
    res = 800,
    units = "in",
    width = 5,
    height = 3
  )

  suppressWarnings(print(gg))

  grDevices::dev.off()

  #Rdata
  create_folder = paste(getwd(), "Rdata", sep = "/")
  if (!dir.exists(create_folder)){
    dir.create(create_folder, showWarnings = FALSE)
  }
  Rdata_dir = create_folder

  filename = "Mahalanobis_Outliers.csv"
  pathname = paste(Rdata_dir, filename, sep = "/")

  utils::write.csv(file = pathname, x = maha2)

  return(get_outliers)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Remove outliers
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param outliers A character vector containing the samples, which should be removed. These should match the column names of the sample table.
#' @returns Returns a sampleTable without the defined columns.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #remove outliers from input dataframe
#' #a vector of the column names to be removed is used for this
#' #vector can be obtained using the function identify_outliers
#' dat_replicates_processed_short = remove_outliers(sampleTable = dat_replicates_processed,
#' outliers = c("double_1", "ctrl_4"))
#'
#' ##remove outliers from input dataframe
#' ##a vector of the column names to be removed is used for this
#' ##vector can be obtained using the function identify_outliers
#' #dat_replicates_processed_short = remove_outliers(sampleTable = dat_replicates_processed,
#' #outliers = c("double_1"))
#'
#' setwd(savewd)
#' @export
remove_outliers <- function(sampleTable, outliers) {

  tryCatch({

  for (i in 1:length(outliers)){

    tryCatch({

    sampleTable = sampleTable[, which(grepl(colnames(sampleTable), pattern = paste(outliers[i], "$", sep = "")) == FALSE)]

    }, error = function(e) {
      cat("ERROR:", conditionMessage(e), outliers[i], "\n")
    })

  }

  return(sampleTable)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Log2 transformation
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different conditions as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param export_csv TRUE or FALSE. TRUE is default and exports a csv file containing the median-normalized data to a subfolder named Rdata, which is created within your working directory by this function.
#' @returns Returns a dataframe containing log2-transformed data.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #no export of the results
#' dat_log2 = log2_transform(sampleTable = dat_FC_replicates, export_csv = FALSE)
#'
#' ##export csv with normalized data to a folder called Rdata within your working directory
#' #dat_log2 = log2_transform(sampleTable = dat_FC_replicates)
#'
#' setwd(savewd)
#' @export
log2_transform <- function(sampleTable, export_csv = TRUE) {

  tryCatch({

  dat_log2 = log2(sampleTable)

  if(export_csv == TRUE){

    create_folder = paste(getwd(), "Rdata", sep = "/")

    if (!dir.exists(create_folder)){
      dir.create(create_folder, showWarnings = FALSE)
    }

    Rdata_dir = create_folder

    filename = paste(Rdata_dir, "dat_log2.csv", sep = "/")
    utils::write.csv(file = filename, x = dat_log2)
  }

  return(dat_log2)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Median normalization
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param export_csv TRUE or FALSE. TRUE is default and exports a csv file containing the median-normalized data to a subfolder named Rdata, which is created within your working directory by this function.
#' @returns Returns a dataframe containing median-normalized data.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #prepare data
#' dat_replicates_log2 = log2_transform(sampleTable = dat_FC_replicates, export_csv = FALSE)
#'
#' ##no export of the results
#' #sampleTable_norm = median_normalize(sampleTable = dat_replicates_log2, export_csv = FALSE)
#'
#' ##export csv with normalized data to a folder called Rdata within your working directory
#' #sampleTable_norm = median_normalize(sampleTable = dat_replicates_log2)
#'
#' setwd(savewd)
#' @export
median_normalize <- function(sampleTable, export_csv = TRUE) {

  tryCatch({

  dat_to_normalize = sampleTable

  nSamples = dim(dat_to_normalize)[2]
  sample_vector = colnames(dat_to_normalize)

  # create empty vector that will be filled with median values
  median_abundance_vector = vector(mode = "expression", length = nSamples)
  for (i in 1:nSamples){
    M = stats::na.omit(dat_to_normalize[,i])
    median_abundance_vector[i] = stats::median(M)
  }

  #normalize to 0
  for (i in 1:nSamples){
    M = stats::na.omit(dat_to_normalize[,i])
    dat_median = stats::median(M)
    M_norm = dat_to_normalize[,i] - dat_median
    assign(x = paste("dat_norm", sample_vector[i], sep = "_"), value = M_norm)
  }

  dat_norm = do.call(cbind, lapply(ls(pattern = "dat_norm_"),
                                   function(x){
                                     get_dat = get(x)
                                     get_dat
                                   }))

  dat_norm = as.data.frame(dat_norm)

  sample_vector = as.vector(ls(pattern = "dat_norm_"))
  sample_vector = gsub("dat_norm_", "", sample_vector)
  colnames(dat_norm) = sample_vector
  row.names(dat_norm) = row.names(dat_to_normalize)

  if(export_csv == TRUE){

    create_folder = paste(getwd(), "Rdata", sep = "/")

    if (!dir.exists(create_folder)){
      dir.create(create_folder, showWarnings = FALSE)
    }

    Rdata_dir = create_folder

    filename = paste(Rdata_dir, "dat_norm.csv", sep = "/")
    utils::write.csv(file = filename, x = dat_norm)
  }

  return(dat_norm)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Filter for reliably identified proteins
#'
#' @param sampleTable A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix). Except row names and column names, which should be strings, the dataframe content should be numeric.
#' @param number_replicates_reliable Provide the number of replicates for which quantitation data should be available at least to consider a protein reliably identified under the particular condition. Default is 3.
#' @param reliable_all_comparisons Should be TRUE or FALSE. Default is FALSE, which leads to a dataframe, which contains all proteins identified in the given number of replicates for at least one comparison. If set to TRUE, proteins are returned, which were identified in the given number of replicates for all comparisons.
#' @returns Returns a dataframe containing reliable data.
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #return all proteins reliably identified over all treatments
#' #proteins are considered reliably identified if they are identified in at least 3 replicates
#' dat_reliable_short = get_dat_reliable(sampleTable = dat_replicates_processed,
#'                                       number_replicates_reliable = 3,
#'                                       reliable_all_comparisons = TRUE)
#'
#' ##return all proteins reliably identified in at least one treatment
#' ##proteins are considered reliably identified if they are identified in at least 4 replicates
#' #dat_reliable = get_dat_reliable(sampleTable = dat_replicates_processed,
#'                                 #number_replicates_reliable = 4,
#'                                 #reliable_all_comparisons = FALSE)
#'
#' setwd(savewd)
#' @export
get_dat_reliable <- function(sampleTable, number_replicates_reliable = 3, reliable_all_comparisons = FALSE){

  tryCatch({

  samples = unique(gsub("(.*)_.*","\\1", colnames(sampleTable)))

  for ( i in samples){
    dat_subset = subset(sampleTable, select = grepl(pattern = i, colnames(sampleTable)))

    nReps = dim(dat_subset)[2]

    NACounts = apply(dat_subset, 1, function(x){
      sum(is.na(x))
    })

    nCounts = nReps - NACounts

    assign(x = paste("nProteins", i, sep = "_"), value = nCounts)

  }

  nCounts_df = do.call(cbind, lapply(ls(pattern = "^nProteins_"),
                                     function(x){
                                       get_dat = get(x)
                                     }))

  nCounts_df = as.data.frame(nCounts_df)

  row.names(nCounts_df) = rownames(sampleTable)

  if (reliable_all_comparisons == TRUE){

    Accessions_nCounts_df = as.data.frame(rownames(nCounts_df)[rowSums(nCounts_df >= number_replicates_reliable,
                                                                 na.rm = TRUE) == ncol(nCounts_df)])

    colnames(Accessions_nCounts_df) = c("Accessions")
    rownames(Accessions_nCounts_df) = Accessions_nCounts_df$Accessions

    summary_replicates_short = merge(Accessions_nCounts_df, sampleTable,
                                     by = "row.names", all.x = TRUE)

    rownames(summary_replicates_short) = summary_replicates_short$Row.names
    summary_replicates_short = summary_replicates_short[!grepl(colnames(summary_replicates_short),
                                                               pattern = "Accessions")]

    summary_replicates_short = summary_replicates_short[!grepl(colnames(summary_replicates_short),
                                                               pattern = "Row.names")]

    return(summary_replicates_short)} else {

      Accessions_nCounts_df = as.data.frame(rownames(nCounts_df)[rowSums(nCounts_df >= number_replicates_reliable,
                                                             na.rm = TRUE) > 0])

      colnames(Accessions_nCounts_df) = c("Accessions")
      rownames(Accessions_nCounts_df) = Accessions_nCounts_df$Accessions

      dat_relreps = merge(Accessions_nCounts_df, sampleTable, by = "row.names", all.x = TRUE)

      rownames(dat_relreps) = dat_relreps$Row.names
      dat_relreps = dat_relreps[!grepl(colnames(dat_relreps), pattern = "Accessions")]

      dat_relreps = dat_relreps[!grepl(colnames(dat_relreps), pattern = "Row.names")]

      return(dat_relreps)

    }

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}

#' Summarize regulation counts for p-values and adjusted p-values
#'
#' @param sampleTable Output of the functions calculation_FC_data() or calculation_intensity_data(). Should contain columns for average log2(fold changes), p-values and adjusted p-values following this schema: plusLPS_vs_noLPS_FC, plusLPS_vs_noLPS_pvalue, plusLPS_vs_noLPS_pvalueadj
#' @param significance_cutoff Define the value at which a protein should be considered significantly affected. Default: 0.05
#' @returns Returns a dataframe containing the numbers of significantly affected proteins based on p-values and adjusted p-values
#' @examples
#' #define working directory for output
#' #here, a temporary folder is used
#' savewd = getwd()
#' setwd(tempdir())
#'
#' #return regulation counts for raw p-values and adjusted p-values using a cutoff of 0.05
#' regulation_counts = get_regulation_counts(sampleTable = dat_calculated_LFQ)
#'
#' ##return regulation counts for raw p-values and adjusted p-values using a cutoff of 0.01
#' #regulation_counts = get_regulation_counts(sampleTable = dat_calculated_LFQ,
#' #significance_cutoff = 0.01)
#'
#' setwd(savewd)
#' @export
get_regulation_counts <- function(sampleTable, significance_cutoff = 0.05){

  tryCatch({

  dat_select = as.data.frame(sampleTable[, grepl(colnames(sampleTable), pattern = "_FC$")])
  colnames(dat_select) = colnames(sampleTable)[grepl(colnames(sampleTable),
                                                         pattern = "_FC$")]

  samples = unique(gsub("(.*)_.*","\\1", colnames(dat_select)))

  result_matrix = as.data.frame(matrix(data = NA, nrow = length(samples),
                                       ncol = 8))

  colnames(result_matrix) = c("down_pvalue", "none_pvalue", "up_pvalue", "sum_pvalue",
                              "down_pvalueadj", "none_pvalueadj", "up_pvalueadj",
                              "sum_pvalueadj")

  rownames(result_matrix) = samples

  for (i in 1:length(samples)){

    tryCatch({

    get_dat = sampleTable[, grepl(colnames(sampleTable), pattern = paste(samples[i],
                                                                         "_FC$|",
                                                                         samples[i],
                                                                         "_pvalue", sep = ""))]

    colnames(get_dat) = c("FC", "pv", "padj")

    result_matrix[i, "down_pvalue"] = length(stats::na.exclude(get_dat[get_dat$pv <= significance_cutoff & get_dat$FC < 0,
                                                                       "FC"]))
    result_matrix[i, "none_pvalue"] = length(stats::na.exclude(get_dat[get_dat$pv > significance_cutoff,
                                                                       "FC"]))
    result_matrix[i, "up_pvalue"] = length(stats::na.exclude(get_dat[get_dat$pv <= significance_cutoff & get_dat$FC > 0,
                                                                     "FC"]))

    result_matrix[i, "sum_pvalue"] = sum(result_matrix[i, 1], result_matrix[i, 2], result_matrix[i, 3])

    result_matrix[i, "down_pvalueadj"] = length(stats::na.exclude(get_dat[get_dat$padj <= significance_cutoff & get_dat$FC < 0,
                                                                           "FC"]))
    result_matrix[i, "none_pvalueadj"] = length(stats::na.exclude(get_dat[get_dat$padj > significance_cutoff,
                                                                           "FC"]))
    result_matrix[i, "up_pvalueadj"] = length(stats::na.exclude(get_dat[get_dat$padj <= significance_cutoff & get_dat$FC > 0,
                                                                         "FC"]))

    result_matrix[i, "sum_pvalueadj"] = sum(result_matrix[i, 5], result_matrix[i, 6], result_matrix[i, 7])

    }, error = function(e) {
      cat("ERROR:", conditionMessage(e), i, "\n")
    })

  }

  return(result_matrix)

  }, error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
  })

}


