% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/1_FC_data_processing.R
\name{clean_dat_FC}
\alias{clean_dat_FC}
\title{Clean fold change data}
\usage{
clean_dat_FC(sampleTable)
}
\arguments{
\item{sampleTable}{A dataframe with unique identifiers (e.g. Uniprot accessions or UniprotAccession_Site) as row names and the different comparisons as column names (replicates should be indicated using _1, _2, and so on as suffix, comparisons should be indicated using the following schema: treatment_vs_control, of which the "_vs_" is the relevant part). Except row names and column names, which should be strings, the dataframe content should be numeric.}
}
\value{
A dataframe containing data, where fold changes >= 100 as well as 0 data haven been replaced by NA.
}
\description{
Clean fold change data
}
\examples{
sampleTable = clean_dat_FC(sampleTable = dat_FC_replicates)
}
