% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{human_uniprot_fasta}
\alias{human_uniprot_fasta}
\title{Human reference proteome
Fasta file containing the human reference proteome downloaded 4th March 2022 from the UniprotKB and filtered for the proteins identified within the phosphoproteome being provided with this package.}
\format{
## `human_uniprot_fasta`
A data frame with 950 (originally 79052) rows (reflecting the different proteins actually part of the phosphoproteome investigated here) and 2 columns (containing protein and sequence information).
}
\usage{
data(human_uniprot_fasta)
}
\description{
Human reference proteome
Fasta file containing the human reference proteome downloaded 4th March 2022 from the UniprotKB and filtered for the proteins identified within the phosphoproteome being provided with this package.
}
\author{
Isabel Karkossa \email{isabel.karkossa@ufz.de}
}
\keyword{datasets}
